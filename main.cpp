// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  PS Move tracking demo main
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "conefitting.h"
#include "imagehandling.h"

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>  // for cv::VideoCapture
#include <opencv2/highgui.hpp>  // for cv::imshow, cv::waitKey
#include <opencv2/imgproc.hpp>  // for cv::rectangle

#include <iostream>
#include <numeric>

// Hardcoded from one of Ryan's PS4 cameras, left sensor
static const double s_intrins[] = {
    420.70247885, 0., 317.76101911, 0., 425.1910435, 206.74380983, 0., 0., 1.,
};

static const cv::Matx33d intrinsics(s_intrins);
static const cv::Matx<double, 5, 1> distort(0.0320084, -0.09456607, -0.00270804, -0.00235282, 0.07053029);

static cv::VideoWriter writer;

static Contour combineContours(std::vector<Contour>& contours)
{
    Contour allPixels;
    if (contours.size() == 1) {
        allPixels = std::move(contours[0]);
        // avoid undef behavior in case we accidentally access it outside this function.
        contours[0].clear();
    }
    else if (contours.size() > 1) {
        auto totalPixels = std::accumulate(contours.begin(), contours.end(), size_t(0),
                                           [](size_t prevSize, Contour const& c) { return prevSize + c.size(); });
        allPixels.reserve(totalPixels);
        for (const auto& contour : contours) {
            allPixels.insert(allPixels.end(), contour.begin(), contour.end());
        }
        std::cout << "We found " << contours.size() << " contours with a total of " << totalPixels << " pixels.\n";
    }
    return allPixels;
}

static std::vector<cv::Vec2f> makeContourFloat(Contour const& allPixels)
{

    std::vector<cv::Vec2f> points;
    points.reserve(allPixels.size());
    for (const auto& pt : allPixels) {
        points.emplace_back(pt.x, pt.y);
    }
    return points;
}

constexpr float radius = 0.043f / 2.f;
constexpr size_t minPoints = 6;

void plot_points_by_index(cv::Mat& frame, Contour const& allPixels, std::vector<size_t> const& indices, cv::Vec3b color)
{
    for (auto index : indices) {
        auto px = allPixels[index];
        frame.at<cv::Vec3b>(px.y, px.x) = color;
    }
}

bool fit_and_show(cv::Mat frame, Contour const& allPixels, std::vector<cv::Vec3f> const& directions, int waitDuration)
{
    cv::Vec3f position;
    std::vector<size_t> indices;
    ConeFitter coneFitter;
    if (coneFitter.fit_cone_and_get_inlier_indices(directions, minPoints, radius, position, indices)) {
        std::cout << "Position estimated as " << position << "\n";
        plot_points_by_index(frame, allPixels, indices, cv::Vec3b(0, 255, 0));

        cv::Vec3d projectedCenter = intrinsics * cv::Vec3d(position);
        cv::Point2d origPt(projectedCenter[0] / projectedCenter[2], projectedCenter[1] / projectedCenter[2]);
        // this seems weird...
        cv::Point2d pt = cv::Point2d(frame.cols, frame.rows) - origPt;
        cv::drawMarker(frame, pt, cv::Scalar(0, 0, 200), cv::MARKER_CROSS);
        if (writer.isOpened()) {
            writer << frame;
        }
    }
    cv::imshow("Frame", frame);

    auto key = cv::waitKey(waitDuration);
    return key != 'q';
}

bool fit_and_show_all_steps(cv::Mat frame, Contour const& allPixels, std::vector<cv::Vec3f> const& directions, int waitDuration)
{
    cv::Vec3f position;
    std::vector<std::vector<size_t>> allIndices;
    ConeFitter coneFitter;

    if (coneFitter.fit_cone_and_get_step_inlier_indices(directions, minPoints, radius, position, allIndices)) {
        std::cout << "Position estimated as " << position << "\n";
        auto numSteps = allIndices.size();
        std::vector<cv::Mat> frames;
        frames.reserve(numSteps);
        for (size_t i = 0; i < numSteps; ++i) {
            cv::Mat curFrame = frame.clone();
            plot_points_by_index(curFrame, allPixels, allIndices[i], cv::Vec3b(0, 255, 0));

            frames.emplace_back(curFrame);
        }

        cv::vconcat(frames, frame);
        if (writer.isOpened()) {
            writer << frames.back();
        }
    }
    cv::imshow("Frame", frame);

    auto key = cv::waitKey(waitDuration);
    return key != 'q';
}
static void cover_up_noise(cv::Mat& image)
{
    // blank out some noise in this data
    //! @todo Remove this when we can cope with noise
    cv::rectangle(image, cv::Rect(437, 0, 78, 35), cv::Scalar(0), cv::FILLED);
}
bool handle_frame(cv::Mat frame, NormalizedCoords const& cache, int waitDuration, bool showSteps)
{
    cv::Mat singleChannel;
    if (frame.channels() == 3) {
        // It's really mono, but some builds load it like this
        cover_up_noise(frame);
        cv::extractChannel(frame, singleChannel, 0);
    }
    else {
        assert(frame.channels() == 1);
        singleChannel = frame;
        cover_up_noise(singleChannel);
        frame.create(singleChannel.size(), CV_8UC3);
        cv::insertChannel(singleChannel, frame, 0);
        cv::insertChannel(singleChannel, frame, 1);
        cv::insertChannel(singleChannel, frame, 2);
    }

    assert(frame.channels() == 3);
    assert(singleChannel.channels() == 1);
    assert(frame.size() == singleChannel.size());

    auto contours = getContours(singleChannel);

    auto allPixels = combineContours(contours);
    if (allPixels.empty()) {
        std::cout << "No contours?\n";
        return false;
    }
    auto points = makeContourFloat(allPixels);
#if 0
    auto directions = getNormalized3DVectors(points, intrinsics, distort);
#else
    std::vector<cv::Vec3f> directions;
    for (const auto& point : points) {
        cv::Vec3d normalizedVec = cache.getNormalizedVector(point);
        directions.emplace_back(normalizedVec);
    }
#endif

    if (showSteps) {
        return fit_and_show_all_steps(frame, allPixels, directions, waitDuration);
    }
    return fit_and_show(frame, allPixels, directions, waitDuration);
}

int main(int argc, char* argv[])
{
    std::cout << "Camera intrinsics:\n";
    std::cout << intrinsics << std::endl;

    // Handle command line args
    int waitDuration = 20;
    bool steps = false;
    const auto defaultFnToLoad = "data/psmove_%04d.png";
    const auto defaultFileToWrite = "output.mp4";
    std::string fnToLoad;
    std::string fileToWrite;
    for (int i = 1; i < argc; ++i) {
        std::string arg{argv[i]};
        if (arg == "--steps" || arg == "-s") {
            steps = true;
            std::cout << "Showing all iteration steps.\n";
        }
        else if (arg == "--pause" || arg == "-p") {
            waitDuration = 0;
            std::cout << "Pausing at each frame.\n";
        }
        else if (arg == "--write" || arg == "-w") {
            fileToWrite = defaultFileToWrite;
            std::cout << "Writing annotated video to " << fileToWrite << ".\n";
        }
        else if (arg == "--help" || arg == "-h") {
            std::cout << "By default, loads video/image sequence " << defaultFnToLoad << "\n";
            std::cout << "Pass a filename/pattern to change which.\n";
            std::cout << "Pass --steps or -s to see every step of the iteration.\n";
            std::cout << "Pass --pause or -p to pause at each frame instead of auto-advancing.\n";
            std::cout << "Pass --write or -w to write the annotated frame output (of the last step, if applicable).\n";
            std::cout << "Pass --help or -h to get this help message.\n";
            return -1;
        }
        else {
            std::cout << "Assuming '" << arg << "' is a video or image filename, or image sequence file pattern.\n";
            if (!fnToLoad.empty()) {
                std::cout << "Already specified " << fnToLoad << " - can only take one file name or file pattern per run.\n";
                return -2;
            }
            fnToLoad = arg;
        }
    }
    if (fnToLoad.empty()) {
        std::cout << "Loading default video/image sequence " << defaultFnToLoad << "\n";
        fnToLoad = defaultFnToLoad;
    }

    std::unique_ptr<NormalizedCoords> cache;
    // This handles videos, images, and sequences of images seamlessly.
    auto cap = cv::VideoCapture(fnToLoad);
    cv::Mat frame;
    bool continueLoop = true;
    while (continueLoop && cap.read(frame)) {
        if (!fileToWrite.empty() && !writer.isOpened()) {
            // Open here because we need the frame size.
            writer.open(fileToWrite, cv::VideoWriter::fourcc('H', '2', '6', '4'), 30, frame.size());
        }
        if (!cache) {
            cache.reset(new NormalizedCoords(frame.size(), intrinsics, distort));
        }
        continueLoop = handle_frame(frame, *cache, waitDuration, steps);
    }
    if (writer.isOpened()) {
        writer.release();
    }
    return 0;
}
