// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Image handling for PS Move stuff.
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "imagehandling.h"

#include <opencv2/imgproc.hpp>  // for findContours
#include <opencv2/calib3d.hpp>  // for undistortPoints in OpenCV4

#include <iostream>

std::vector<Contour> getContours(cv::Mat frame)
{
    std::vector<Contour> output;
    cv::Mat hierarchy;  // not used
    // cv::Mat binary = cv::Mat::zeros(frame.size(), CV_8UC1);

    // cv::threshold(frame, binary, 100, 255, cv::THRESH_BINARY);

    // std::cout << "frame:" << cv::typeToString(frame.type()) << std::endl;
    // std::cout << "allegedly binary:" << cv::typeToString(binary.type()) << std::endl;

    //! @todo we can probably do better than this generic algorithm.
    //! @todo we also need to return the associated "outside" pixel so we can get the "between" vector
    cv::findContours(frame, output, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);
    return output;
}

NormalizedCoords::NormalizedCoords(cv::Size size, const cv::Matx33d& intrinsics, const cv::Matx<double, 5, 1>& distortion)
    : cacheX_(size, CV_32FC1), cacheY_(size, CV_32FC1)
{
    std::vector<cv::Vec2f> inputCoords;
    const auto n = size.width * size.height;
    // Prepare a list of all integer coordinates
    inputCoords.reserve(n);
    for (int row = 0; row < size.height; ++row) {
        for (int col = 0; col < size.width; ++col) {
            inputCoords.emplace_back(col, row);
        }
    }
    // Undistort/reproject those coordinates in one call, to make use of cached internal/intermediate computations.
    std::vector<cv::Vec2f> outputCoords;
    outputCoords.reserve(n);
    cv::undistortPoints(inputCoords, outputCoords, intrinsics, distortion);

    // Populate the cache matrices
    for (int i = 0; i < n; ++i) {
        auto input = cv::Point{int(inputCoords[i][0]), int(inputCoords[i][1])};
        cacheX_.at<float>(input) = outputCoords[i][0];
        cacheY_.at<float>(input) = outputCoords[i][1];
    }
}

cv::Vec2f NormalizedCoords::getNormalizedImageCoords(cv::Point2f origCoords) const
{
    cv::Mat patch;
    cv::getRectSubPix(cacheX_, cv::Size(1, 1), origCoords, patch);
    auto x = patch.at<float>(0, 0);
    cv::getRectSubPix(cacheY_, cv::Size(1, 1), origCoords, patch);
    auto y = patch.at<float>(0, 0);
    return {x, y};
}

cv::Vec3f NormalizedCoords::getNormalizedVector(cv::Point2f origCoords) const
{
    // cameras traditionally look along -z, so we want negative sqrt
    auto pt = getNormalizedImageCoords(origCoords);
    auto z = -std::sqrt(1.f - pt.dot(pt));
    return {pt[0], pt[1], z};
}

std::vector<cv::Vec3f> getNormalized3DVectors(std::vector<cv::Vec2f> const& points, const cv::Matx33d& intrinsics,
                                              const cv::Matx<double, 5, 1>& distortion)
{
    //! @todo we could pre-compute the undistorted/normalized-projection coordinates for each pixel
    std::vector<cv::Vec2f> outputPoints;
    cv::undistortPoints(points, outputPoints, intrinsics, distortion);

    // Compute the associated z coordinate
    std::vector<cv::Vec3f> output;
    output.reserve(outputPoints.size());

    for (const auto& pt : outputPoints) {
#ifdef SET_Z_TO_NEG_1
        // cameras traditionally look along -z
        cv::Vec3f vec(pt[0], pt[1], -1);
        output.emplace_back(cv::normalize(vec));
#else
        // cameras traditionally look along -z, so we want negative sqrt
        auto z = -std::sqrt(1.f - pt.dot(pt));
        output.emplace_back(pt[0], pt[1], z);
#endif
    }
    return output;
}
