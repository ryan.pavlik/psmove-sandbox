// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Image handling for PS Move stuff.
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */
#pragma once

#include <opencv2/core.hpp>

#include <vector>

using Contour = std::vector<cv::Point>;

/*!
 * Takes a binary (single channel) image.
 */
std::vector<Contour> getContours(cv::Mat frame);

std::vector<cv::Vec3f> getNormalized3DVectors(std::vector<cv::Vec2f> const& points, const cv::Matx33d& intrinsics,
                                              const cv::Matx<double, 5, 1>& distortion);

/*!
 * @brief Provides cached, precomputed access to normalized image coordinates from original, distorted ones.
 */
class NormalizedCoords
{
public:
    //! Set up the precomputed cache for a given camera.
    NormalizedCoords(cv::Size size, const cv::Matx33d& intrinsics, const cv::Matx<double, 5, 1>& distortion);

    /*!
     * @brief Get normalized, undistorted coordinates from a point in the original (distorted, etc.) image.
     */
    cv::Vec2f getNormalizedImageCoords(cv::Point2f origCoords) const;

    /*!
     * @brief Get normalized vector in the camera-space direction corresponding to the original (distorted, etc.) image coordinates.
     * 
     * Note that the Z component will be negative by convention.
     */
    cv::Vec3f getNormalizedVector(cv::Point2f origCoords) const;

private:
    cv::Mat cacheX_;
    cv::Mat cacheY_;
};
